var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

module.exports = new Schema({
    title:  { type: String, required: true },
    amount: { type: Number, required: true },
    date:   { type: Date  , required: true }
});
