var mongoose = require('mongoose'),
    Payment = require('../lib/PaymentSchema');

Payment.virtual('created').get(function() {
    return this._id.getTimestamp();
});

module.exports = mongoose.model('Payment', Payment);
