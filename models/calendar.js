var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    PaymentSchema = require('../lib/PaymentSchema');

var Calendar = new Schema({
    facebook: { type: String, required: true },
    email:    { type: String, required: true },
    title:    { type: String, required: true },
    payments: [PaymentSchema]
});

Calendar.virtual('created').get(function() {
    return this._id.getTimestamp();
});

Calendar.statics.findOrCreate = function (facebook, email, cb) {
    var self = this;
    this.findOne({'facebook': facebook}, function (err, cal) {
        if (err) console.log(err);
        if (!cal) {
            cal          = new self();
            cal.facebook = facebook;
            cal.email    = email;
            cal.title    = 'Calendar title';
            cal.save();
        }
        cb(cal);
    });
}

module.exports = mongoose.model('Calendar', Calendar);
