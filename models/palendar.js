var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Palendar = new Schema({
    title: { type: String, required: true },
    amount: { type: Number, required: true }
});

module.exports = mongoose.model('Palendar', Palendar);