require('coffee-script/register');
session = require('express-session');

var express = require('express'),
  config = require('./config/config'),
  db = require('./app/models');

var app = express();

app.use(session({
    secret: config.sessionSecret,
    resave: false,
    saveUninitialized: true,
    cookie: {
        path: '/',
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24 * 30
    }
}));

app.use(function (req, res,next) {
    req.isAuth = function () {
        return typeof req.session.facebook == 'object';
    }
    next();
});

require('./config/express')(app, config);
require('./helpers')(app);

//db.sequelize
//  .sync()
//  .then(function () {
    app.listen(config.port);
//  }).catch(function (e) {
//    throw new Error(e);
//  });
