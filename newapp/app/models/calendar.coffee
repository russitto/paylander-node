module.exports = (sequelize, DataTypes) ->

  Calendar = sequelize.define 'Calendar',
    fbid: DataTypes.STRING,
    title: DataTypes.STRING,
  ,
    classMethods:
      associate: (models) ->
        Calendar.hasMany models.Payment,
          as: 'Payments'
