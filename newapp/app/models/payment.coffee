module.exports = (sequelize, DataTypes) ->

  Payment = sequelize.define 'Payment',
    title: DataTypes.STRING,
    paid:
      type: DataTypes.BOOLEAN
      defaultValue: false
    date:
      type: DataTypes.DATE
      defaultValue: sequelize.NOW
  ,
    classMethods:
      associate: (models) ->
        Payment.belongsTo models.Calendar
