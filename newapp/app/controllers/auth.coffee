express = require 'express'
db      = require '../models'
crypto  = require 'crypto'
config  = require '../../config/config'
request = require 'request'

module.exports = (app) ->
  app.get '/auth/login', (req, res) ->
    proto = if req.https == 'on' or req.headers['x-forwarded-proto'] == 'https' then 'https' else 'http'
    base = proto + '://' + req.headers.host
    state = ''
    crypto.randomBytes 16, (ex, buf) ->
      state = buf.toString('hex')
      # @TODO port config.port
      fbUrl = 'http://www.facebook.com/dialog/oauth' + '?client_id=' + config.facebook.id + '&redirect_uri=' + base + '/auth/cb' + '&state=' + state + '&scope=email'
      res.redirect fbUrl
      return
    return
  app.get '/auth/cb', (req, res) ->
    code = req.query.code
    proto = if req.https == 'on' or req.headers['x-forwarded-proto'] == 'https' then 'https' else 'http'
    base = proto + '://' + req.headers.host
    fbUrl = 'https://graph.facebook.com/v2.3/oauth/access_token' + '?client_id=' + config.facebook.id + '&client_secret=' + config.facebook.secret + '&code=' + code + '&redirect_uri=' + base + '/auth/cb' + '&grant=client_credentials'
    request {
      url: fbUrl
      json: true
    }, (error, rp, body) ->
      access_token = body.access_token
      url = 'https://graph.facebook.com/v2.3/me' + '?access_token=' + access_token
      request {
        url: url
        json: true
      }, (error, rp, body) ->
        sess = req.session
        body.access_token = access_token
        sess.facebook = body
        res.redirect '/calendar'
        return
      return
    return
  app.get '/auth/logout', (req, res) ->
    req.session.destroy()
    res.redirect '/'
    return
  return
