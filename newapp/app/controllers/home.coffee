express  = require 'express'
router = express.Router()
db = require '../models'

module.exports = (app) ->
  app.use '/', router

router.get '/', (req, res, next) ->
  if req.isAuth()
    res.redirect '/calendar'
  else
    res.render 'index',
      title: 'Vamooo'

router.get '/calendar', (req, res, next) ->
  if !req.isAuth()
    res.redirect '/'
  else
    db.Calendar.findOrCreate
      where:
        fbid: req.session.facebook.id
      defaults:
        fbid: req.session.facebook.id
        title: req.session.facebook.first_name + ' Calendar'
    .spread (calendar, created) ->
      cal = calendar.get
        plain: true
      db.Payment.findAll
        order: ['date']
      .then (payments) ->
        res.render 'calendar',
          calendar: cal
          payments: payments
          payment: db.Payment.build
            date: Date.now()
          facebook: req.session.facebook

router.post '/calendar', (req, res, next) ->
  if !req.isAuth()
    res.redirect '/'
  else
    db.Calendar.findOrCreate
      where:
        fbid: req.session.facebook.id
      defaults:
        fbid: req.session.facebook.id
        title: req.body.title
    .spread (calendar, created) ->
      if !created
        calendar.update
          title: req.body.title
        .then (affectedRows) ->
          res.redirect '/calendar'
      else
        res.redirect '/calendar'

router.post '/calendar/payment', (req, res, next) ->
  if !req.isAuth()
    res.redirect '/'
  else
    db.Calendar.findOrCreate
      where:
        fbid: req.session.facebook.id
      defaults:
        fbid: req.session.facebook.id
        title: req.body.title
    .spread (calendar, created) ->
      payment: db.Payment.build(
        CalendarId: calendar.id
        title: req.body.title
        title: req.body.title
        amount: req.body.amout
        date: req.body.date
      ).save().then (another) ->
        res.redirect '/calendar'
      .catch (error) ->
        console.log 'Error @ payment create'
        res.end 'mmm'
