path     = require 'path'
rootPath = path.normalize __dirname + '/..'
env      = process.env.NODE_ENV || 'development'

config =
  development:
    root: rootPath
    app:
      name: 'paylander-node'
    port: 8080
    db: 'mysql://paylander:paylander@localhost/paylander_node_development'
    sessionSecret: 'La re conchis lora 2013'
    facebook:
      id: '1555911771356746',
      secret: '0522f0570b0c23953596e958d3ceb667'

  test:
    root: rootPath
    app:
      name: 'paylander-node'
    port: 8080
    db: 'mysql://localhost/paylander_node_test'

  production:
    root: rootPath
    app:
      name: 'paylander-node'
    port: 8080
    db: 'mysql://localhost/paylander_node_production'

module.exports = config[env]
