var Payment = require('../models/payment')
  , Calendar = require('../models/calendar')
  , mapper = require('../lib/model-mapper')
  ;

module.exports = function(app) {

    app.get('/calendar', function(req, res) {
        if (typeof req.session.facebook != 'object') {
            res.redirect('/');
        }

        Calendar.findOrCreate(req.session.facebook.id, req.session.facebook.email, function (calendar) {
            var payment = new Payment();
            payment.date = new Date();
            res.render('calendar', {
                facebook: req.session.facebook,
                payment : payment,
                calendar: calendar
            });
        });

    });

    app.post('/calendar', function(req, res) {
        Calendar.findOrCreate(req.session.facebook.id, req.session.facebook.email, function (calendar) {
            calendar.title = req.body.title;
            calendar.save();
            res.redirect('/calendar');
        });
    });

    app.post('/calendar/payment', function(req, res) {
        Calendar.findOrCreate(req.session.facebook.id, req.session.facebook.email, function (calendar) {
            var payment = new Payment(req.body);
            calendar.payments.push(payment);
            calendar.save(function(err) {
                if (err) {
                    // @TODO try again
                    res.end('errroooooor');
                } else {
                    res.redirect('/calendar');
                }
            });
        });
    });
}
