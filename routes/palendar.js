var Palendar = require('../models/palendar'),
    mapper = require('../lib/model-mapper');

module.exports = function(app) {

    app.param('palendarId', function(req, res, next, id) {
        Palendar.findById(id, function(err, palendar) {
            if (err) {
                next(err);
            } else {
                res.locals.palendar = palendar;
                next();
            }
        });
    });
    
    app.get('/palendars', function(req, res) {
        Palendar.find({}, function(err, palendars) {
            res.render('palendar/index', { palendars : palendars });
        });
    });

    app.get('/palendars/create', function(req, res) {
        res.render('palendar/create', { palendar : new Palendar() });
    });

    app.post('/palendars/create', function(req, res) { 
        var palendar = new Palendar(req.body);

        palendar.save(function(err) {
            if (err) {
                res.render('palendar/create', {
                    palendar : palendar
                });
            } else {
                res.redirect('/palendars');
            }
        });
    });

    app.get('/palendars/:palendarId/edit', function(req, res) {
        res.render('palendar/edit');
    });

    app.post('/palendars/:palendarId/edit', function(req, res) {
        mapper.map(req.body).to(res.locals.palendar);

        res.locals.palendar.save(function(err) {
            if (err) {
                res.render('palendar/edit');
            } else {
                res.redirect('/palendars');
            }
        });
    });

    app.get('/palendars/:palendarId/detail', function(req, res) {
        res.render('palendar/detail');
    });

    app.get('/palendars/:palendarId/delete', function(req, res) {
        res.render('palendar/delete');
    });

    app.post('/palendars/:palendarId/delete', function(req, res) {
        Palendar.remove({ _id : req.params.palendarId }, function(err) {
            res.redirect('/palendars');
        });
    });
}

// Used to build the index page. Can be safely removed!
module.exports.meta = {
    name : 'Palendar',
    route : '/palendars'
}
