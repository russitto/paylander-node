var config   = require('../config'),
    request  = require('request'),
    crypto   = require('crypto');

module.exports = function(app) {
    app.get('/auth/login', function(req, res) {
        var proto =  req.https === 'on' || req.headers['x-forwarded-proto'] === 'https'? 'https': 'http';
        var base = proto + '://' + req.headers.host;
        var state = '';
        crypto.randomBytes(16, function (ex, buf) {
            state = buf.toString('hex');
            // @TODO port config.port
            var fbUrl = 'http://www.facebook.com/dialog/oauth'
                      + '?client_id=' + config.facebook.id
                      + '&redirect_uri=' + base  +'/auth/cb'
                      + '&state=' + state
                      + '&scope=email'
                      ;
            res.redirect(fbUrl);
        });
    });

    app.get('/auth/cb', function(req, res) {
        var code = req.query.code;
        var proto =  req.https === 'on' || req.headers['x-forwarded-proto'] === 'https'? 'https': 'http';
        var base = proto + '://' + req.headers.host;
        var fbUrl = 'https://graph.facebook.com/v2.3/oauth/access_token'
                  + '?client_id=' + config.facebook.id
                  + '&client_secret=' + config.facebook.secret
                  + '&code=' + code
                  + '&redirect_uri=' + base  +'/auth/cb'
                  + '&grant=client_credentials'
                  ;
        request({url: fbUrl, json: true}, function (error, rp, body) {
            var access_token = body.access_token;
            var url = 'https://graph.facebook.com/v2.3/me'
                      + '?access_token=' + access_token
                      ;
            request({url: url, json: true}, function (error, rp, body) {
                var sess = req.session;
                body.access_token = access_token;
                sess.facebook = body;
                res.redirect('/calendar');
            });
        });
    });

    app.get('/auth/logout', function(req, res) {
        req.session.destroy();
        res.redirect('/');
    });
}

// Used to build the index page. Can be safely removed!
module.exports.meta = {
    name : 'Auth',
    route : '/auth'
}
